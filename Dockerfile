
FROM maven:3.6.1-jdk-8-alpine
RUN apk add ca-certificates
COPY src /usr/src/app/src
COPY pom.xml /usr/src/app
RUN mvn -f /usr/src/app/pom.xml clean package

FROM openjdk:8-alpine
# TODO: Copy our jar file from the first stage image
COPY --from=0 "/usr/src/app/target/flighttracker-1.0.0-SNAPSHOT.jar" "/usr/src/app/target/flighttracker-1.0.0-SNAPSHOT.jar"
EXPOSE 8080
ENTRYPOINT ["java","-jar","/usr/src/app/target/flighttracker-1.0.0-SNAPSHOT.jar"]
